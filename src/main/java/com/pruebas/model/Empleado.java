package com.pruebas.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Empleado {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id; 

	@Column
	private String primerApellido;
	
	@Column
	private String segundoApellido;
	
	@Column
	private String primerNombre;
	
	@Column
	private String otrosNombres;
	
	@Column
	private String paisEmpleo;
	
	@Column
	private String tipoIdentificacion;
	
	@Column
	private String numeroIdentificacion;
	
	@Column
	private String correoElectronico;
	
	@Column
	@Temporal(TemporalType.DATE)
	private java.util.Date fechaIngreso;
		
	@Column
	private String area;
		
	@Column
	private String estado;	
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date fechaHoraRegistro;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date fechaHoraEdicion;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getOtrosNombres() {
		return otrosNombres;
	}

	public void setOtrosNombres(String otrosNombres) {
		this.otrosNombres = otrosNombres;
	}

	public String getPaisEmpleo() {
		return paisEmpleo;
	}

	public void setPaisEmpleo(String paisEmpleo) {
		this.paisEmpleo = paisEmpleo;
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public java.util.Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(java.util.Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public java.util.Date getFechaHoraRegistro() {
		return fechaHoraRegistro;
	}

	public void setFechaHoraRegistro(java.util.Date fechaHoraRegistro) {
		this.fechaHoraRegistro = fechaHoraRegistro;
	}

	public java.util.Date getFechaHoraEdicion() {
		return fechaHoraEdicion;
	}

	public void setFechaHoraEdicion(java.util.Date fechaHoraEdicion) {
		this.fechaHoraEdicion = fechaHoraEdicion;
	}	

}