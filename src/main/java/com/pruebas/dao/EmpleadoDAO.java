package com.pruebas.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pruebas.model.Empleado;

public interface EmpleadoDAO extends JpaRepository<Empleado, Integer> {

	public List<Empleado> findByPrimerApellido(String primerApellido);
	public List<Empleado> findBySegundoApellido(String segundoApellido);
	public List<Empleado> findByPrimerNombre(String primerNombre);
	public List<Empleado> findByOtrosNombres(String otrosNombres);
	public List<Empleado> findByTipoIdentificacion(String tipoIdentificacion);
	public List<Empleado> findByNumeroIdentificacion(String numeroIdentificacion);
	public List<Empleado> findByPaisEmpleo(String paisEmpleo);
	public List<Empleado> findByCorreoElectronico(String correoElectronico);

}