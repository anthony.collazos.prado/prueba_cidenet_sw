package com.pruebas.rest;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pruebas.dao.EmpleadoDAO;
import com.pruebas.model.Empleado;
import com.pruebas.service.EmpleadoService;

@RestController
@RequestMapping("empleados")
public class EmpleadoRest {

	@Autowired
	private EmpleadoDAO empleadoDAO;
	@Autowired
	private EmpleadoService empleadoService;
	
	@PostMapping("/guardar")
	public void guardar(@RequestBody Empleado empleado) {
		empleadoDAO.save(empleado);
	}
	
	@GetMapping("/listar")
	public List<Empleado> listar() {
		return empleadoDAO.findAll();
	}
	
	@DeleteMapping("/eliminar/{id}")
	public void eliminar(@PathVariable("id") Integer id) {
		empleadoDAO.deleteById(id);
	}
	
	@PutMapping("/actualizar")
	public void actualizar(@RequestBody Empleado empleado) {
		empleadoDAO.save(empleado);
	}

	@GetMapping("/verificar")
	public List<Empleado> verificar() {		
		return empleadoDAO.findAll();
	}	
	
	@GetMapping("/primerapellido/{primerApellido}")
	public List<Empleado> getEmpleadosByPrimerApellido(@PathVariable("primerApellido") String primerApellido) {
		return empleadoService.getEmpleadosByPrimerApellido(primerApellido);
	}
	
	@GetMapping("/segundoapellido/{segundoApellido}")
	public List<Empleado> getEmpleadosBySegundoApellido(@PathVariable("segundoApellido") String segundoApellido) {
		return empleadoService.getEmpleadosBySegundoApellido(segundoApellido);
	}
	
	@GetMapping("/primernombre/{primerNombre}")
	public List<Empleado> getEmpleadosByPrimerNombre(@PathVariable("primerNombre") String primerNombre) {
		return empleadoService.getEmpleadosByPrimerNombre(primerNombre);
	}
	
	@GetMapping("/otrosnombres/{otrosNombres}")
	public List<Empleado> getEmpleadosByOtrosNombres(@PathVariable("otrosNombres") String otrosNombres) {
		return empleadoService.getEmpleadosByOtrosNombres(otrosNombres);
	}

	@GetMapping("/tipoidentificacion/{tipoIdentificacion}")
	public List<Empleado> getEmpleadosByTipoIdentificacion(@PathVariable("tipoIdentificacion") String tipoIdentificacion) {
		return empleadoService.getEmpleadosByTipoIdentificacion(tipoIdentificacion);
	}

	@GetMapping("/numeroidentificacion/{numeroIdentificacion}")
	public List<Empleado> getEmpleadosByNumeroIdentificacion(@PathVariable("numeroIdentificacion") String numeroIdentificacion) {
		return empleadoService.getEmpleadosByNumeroIdentificacion(numeroIdentificacion);
	}

	@GetMapping("/paisempleo/{paisEmpleo}")
	public List<Empleado> getEmpleadosByPaisEmpleo(@PathVariable("paisEmpleo") String paisEmpleo) {
		return empleadoService.getEmpleadosByPaisEmpleo(paisEmpleo);
	}

	@GetMapping("/correoelectronico/{correoElectronico}")
	public List<Empleado> getEmpleadosByCorreoElectronico(@PathVariable("correoElectronico") String correoElectronico) {
		return empleadoService.getEmpleadosByCorreoElectronico(correoElectronico);
	}
}
