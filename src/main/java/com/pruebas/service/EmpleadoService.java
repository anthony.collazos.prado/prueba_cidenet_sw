package com.pruebas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pruebas.dao.EmpleadoDAO;
import com.pruebas.model.Empleado;

@Service
public class EmpleadoService {

	@Autowired
	private EmpleadoDAO empleadoDAO;

	public List<Empleado> getEmpleadosByPrimerApellido(String primerApellido) {
		return empleadoDAO.findByPrimerApellido(primerApellido);
	}
	
	public List<Empleado> getEmpleadosBySegundoApellido(String segundoApellido) {
		return empleadoDAO.findBySegundoApellido(segundoApellido);
	}
	
	public List<Empleado> getEmpleadosByPrimerNombre(String primerNombre) {
		return empleadoDAO.findByPrimerNombre(primerNombre);
	}

	public List<Empleado> getEmpleadosByOtrosNombres(String otrosNombres) {
		return empleadoDAO.findByOtrosNombres(otrosNombres);
	}

	public List<Empleado> getEmpleadosByTipoIdentificacion(String tipoIdentificacion) {
		return empleadoDAO.findByTipoIdentificacion(tipoIdentificacion);
	}

	public List<Empleado> getEmpleadosByNumeroIdentificacion(String numeroIdentificacion) {
		return empleadoDAO.findByNumeroIdentificacion(numeroIdentificacion);
	}

	public List<Empleado> getEmpleadosByPaisEmpleo(String paisEmpleo) {
		return empleadoDAO.findByPaisEmpleo(paisEmpleo);
	}

	public List<Empleado> getEmpleadosByCorreoElectronico(String correoElectronico) {
		return empleadoDAO.findByCorreoElectronico(correoElectronico);
	}
}
